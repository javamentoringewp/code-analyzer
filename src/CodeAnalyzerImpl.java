import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.LinkedList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CodeAnalyzerImpl implements CodeAnalyzer {

	private String dir;
	private String solutionSourceCode;
	private Class<?> solutionClass;
	private final static String solutionClassName = "Solution";
	private final static String CODE_BLOCK = "(\\{(?:(?:[^\\{\\}])*(?:(\\{(?:(?:[^\\{\\}])*(?:\\{(?:(?:[^\\{\\}])*(?:\\{[^\\{\\}]*\\})*(?:[^\\{\\}])*)*\\})*(?:[^\\{\\}])*)*\\}))*(?:[^\\{\\}])*)*\\})";

	public CodeAnalyzerImpl() {
		this.dir = prepareDir();
		this.solutionSourceCode = getSourceCode();
		this.solutionClass = loadClass();
	}

	private String prepareDir() {
		String dir = (new File("")).getAbsolutePath();
		if (!dir.endsWith(System.getProperty("file.separator"))) {
			dir = dir + System.getProperty("file.separator");
		}
		return dir;
	}

	private Class loadClass() {
		ClassLoader classLoader = CodeAnalyzerImpl.class.getClassLoader();
		Class aClass = null;
		try {
			aClass = classLoader.loadClass("Solution");
		} catch (ClassNotFoundException e) {
			System.err.println("Не могу загрузить файл класса решения!");
		}
		return aClass;
	}

	private String getSourceCode() {
		StringBuilder sb = new StringBuilder();
		try {
			BufferedReader reader = new BufferedReader(new FileReader(dir + "Solution.java"));
			String line = reader.readLine();
			while (line != null) {
				sb.append(line);
				line = reader.readLine();
			}
		} catch (IOException e) {
			System.err.println("Не могу получить доступ к файлу исходного кода!");
		}
		return sb.toString();
	}

	private Method getMethod(String methodName, Class<?> returnType, Class<?>[] arguments) {
		Method method;
		try {
			method = solutionClass.getDeclaredMethod(methodName, arguments);
			if (method == null) {
				return null;
			}
			return method.getReturnType().equals(returnType) ? method : null;
		} catch (NoSuchMethodException e) {
			return null;
		}
	}

	@Override
	public boolean hasMethodInClass(String methodName) {
		Method[] methods = solutionClass.getDeclaredMethods();
		for (Method method : methods) {
			if (method.getName().equals(methodName)) {
				return true;
			}
		}
		return false;
	}

	@Override
	public boolean hasMethodInClass(String methodName, Class<?> returnType, Class<?>[] arguments) {
		return getMethod(methodName, returnType, arguments) != null;
	}

	@Override
	public boolean hasMethodInClass(String methodName, Class<?> returnType, Class<?>[] arguments, String[] modifiers) {
		Method method = getMethod(methodName, returnType, arguments);
		if (method == null) {
			return false;
		}
		int counts = 0;
		int actualModifiers = method.getModifiers();

		for (String m : modifiers) {
			switch (m) {
				case "public":
					if (Modifier.isPublic(actualModifiers)) {
						counts++;
					}
					break;
				case "static":
					if (Modifier.isStatic(actualModifiers)) {
						counts++;
					}
					break;
				case "final":
					if (Modifier.isFinal(actualModifiers)) {
						counts++;
					}
					break;
				case "private":
					if (Modifier.isPrivate(actualModifiers)) {
						counts++;
					}
					break;
				case "protected":
					if (Modifier.isProtected(actualModifiers)) {
						counts++;
					}
					break;
				case "abstract":
					if (Modifier.isAbstract(actualModifiers)) {
						counts++;
					}
					break;
			}
		}
		return counts == modifiers.length;
	}

	@Override
	public boolean isPublicMethod(String methodName, Class<?> returnType, Class<?>[] arguments) {
		Method method = getMethod(methodName, returnType, arguments);
		return method != null && Modifier.isPublic(method.getModifiers());
	}

	@Override
	public boolean isPrivateMethod(String methodName, Class<?> returnType, Class<?>[] arguments) {
		Method method = getMethod(methodName, returnType, arguments);
		return method != null && Modifier.isPrivate(method.getModifiers());
	}

	@Override
	public boolean isProtectedMethod(String methodName, Class<?> returnType, Class<?>[] arguments) {
		Method method = getMethod(methodName, returnType, arguments);
		return method != null && Modifier.isProtected(method.getModifiers());
	}

	@Override
	public boolean isFinalMethod(String methodName, Class<?> returnType, Class<?>[] arguments) {
		Method method = getMethod(methodName, returnType, arguments);
		return method != null && Modifier.isFinal(method.getModifiers());
	}

	@Override
	public boolean isStaticMethod(String methodName, Class<?> returnType, Class<?>[] arguments) {
		Method method = getMethod(methodName, returnType, arguments);
		return method != null && Modifier.isStatic(method.getModifiers());
	}

	@Override
	public boolean hasFieldInClass(String fieldName) {
		try {
			solutionClass.getDeclaredField(fieldName);
		} catch (NoSuchFieldException e) {
			return false;
		}
		return true;
	}

	@Override
	public boolean hasFieldInClass(String fieldName, Class<?> fieldClass) {
		try {
			return solutionClass.getDeclaredField(fieldName).getType().equals(fieldClass);
		} catch (NoSuchFieldException e) {
			return false;
		}
	}

	@Override
	public String getText() {
		return solutionSourceCode;
	}

	@Override
	public boolean hasStringInCode(String regex) {
		//TODO нужно ли удалять строковые литералы из исходников
		//String clearSolutionSourceCode = Pattern.compile("\".*\"").matcher(solutionSourceCode).replaceAll("");
		return Pattern.compile(regex).matcher(solutionSourceCode).find();
	}

	private Class<?> getSubClassInClass(String clazzName, Class<?> clazz) {
		for (Class subClazz : clazz.getDeclaredClasses()) {
			if (subClazz.getName().equals(clazzName)) {
				return subClazz;
			}
		}
		return null;
	}

	private Method getMethodInSubClassInClass(String methodName, Class<?> clazz) {
		for (Method method : clazz.getDeclaredMethods()) {
			if (method.getName().equals(methodName)) {
				return method;
			}
		}
		return null;
	}

	private Field getFieldInSubClassInClass(String fieldName, Class<?> clazz) {
		for (Field field : clazz.getDeclaredFields()) {
			if (field.getName().equals(fieldName)) {
				return field;
			}
		}
		return null;
	}

	@Override
	public boolean hasMethodInSubClassInClass(String methodName, String className) {
		Class<?> clazz = getSubClassInClass(className, solutionClass);
		if (clazz == null) {
			return false;
		}
		Method method = getMethodInSubClassInClass(methodName, clazz);
		return method != null;
	}

	@Override
	public boolean hasFieldInSubClassInClass(String fieldName, Class<?> clazz) {
		return getFieldInSubClassInClass(fieldName, clazz) != null;
	}

	@Override
	public boolean hasFieldsInSubClassInClass(String className) {
		Class<?>[] classes = solutionClass.getDeclaredClasses();
		for (Class clazz : classes) {
			if (!clazz.getName().equals(solutionClassName + "$" + className)) {
				continue;
			}
			if (clazz.getDeclaredFields().length > 0) {
				return true;
			}
		}
		return false;
	}

	@Override
	public boolean hasPublicFieldInSubClassInClass(String fieldName, String className) {
		Class<?> clazz = getSubClassInClass(className, solutionClass);
		if (clazz == null) {
			return false;
		}
		Field field = getFieldInSubClassInClass(fieldName, clazz);
		if (field == null) {
			return false;
		}
		return Modifier.isPublic(field.getModifiers());
	}

	@Override
	public boolean hasPublicMethodInSubClassInClass(String methodName, String className) {
		Class<?> clazz = getSubClassInClass(className, solutionClass);
		if (clazz == null) {
			return false;
		}
		Method method = getMethodInSubClassInClass(methodName, clazz);
		if (method == null) {
			return false;
		}
		return Modifier.isPublic(method.getModifiers());
	}

	@Override
	public boolean hasSubClassInClass(String className) {
		Class<?> clazz = getSubClassInClass(className, solutionClass);
		return clazz != null;
	}

	@Override
	public boolean hasPublicSubClassInClass(String className) {
		Class<?> clazz = getSubClassInClass(className, solutionClass);
		if (clazz == null) {
			return false;
		}
		return Modifier.isPublic(clazz.getModifiers());
	}

	@Override
	public boolean hasTextInMethodInSourceCode(String text, String methodName) {
		for (String methodSource : getMatchesByPattern(getMethodPattern(methodName), solutionSourceCode)) {
			methodSource = methodSource.replaceFirst(methodName, "");
			if (Pattern.compile(text).matcher(methodSource).find()) {
				return true;
			}
		}
		return false;
	}

	private List<String> getMatchesByPattern(Pattern pattern, String source) {
		List<String> result = new LinkedList<>();
		Matcher matcher = pattern.matcher(source);
		while (matcher.find()) {
			result.add(matcher.group());
		}
		return result;

	}

	private Pattern getMethodPattern(String methodName) {
		return Pattern.compile("\\s*(\\w+)\\s*(" + methodName + ")\\s*\\((?:\\s*\\w+\\s+\\w+(?:\\,)?)*\\)\\s*" + CODE_BLOCK);
	}

	private Pattern getMethodPattern(Class<?> returnType, String methodName) {
		return Pattern.compile("\\s*[\\w\\.]*(" + returnType.getName() + ")\\s*(" + methodName + ")\\s*\\((?:\\s*\\w+\\s+\\w+(?:\\,)?)*\\)\\s*" + CODE_BLOCK);
	}


	private Pattern getMethodPattern(Class<?> returnType, String methodName, Class<?>[] orderedArgsTypes) {
		StringBuilder builder = new StringBuilder("\\s*[\\w\\.]*(" + returnType.getName() + ")\\s*(" + methodName + ")\\(");
		for (int i = 0; i < orderedArgsTypes.length; i++) {
			Class<?> clazz = orderedArgsTypes[i];
			builder.
					append("[\\w\\.]*(" + clazz.getName() + ")")
					.append("\\s+\\w")
					.append(i != orderedArgsTypes.length - 1 ? "," : "");
		}
		builder.append("\\)\\s*" + CODE_BLOCK);
		return Pattern.compile(builder.toString());
	}

	private Pattern getForPattern() {
		return Pattern.compile("\\s*(for)\\s*\\((([^\\;]*(?:\\;)?){3})\\)\\s*(([^\\{\\}]+\\;)|" + CODE_BLOCK + ")");
	}

	private Pattern geTryCatchPattern() {
		return Pattern.compile("\\s*(try)\\s*(?:\\(.*\\))?\\s*" + CODE_BLOCK + "\\s*(catch)\\s*(\\(.*\\))?\\s*" + CODE_BLOCK +"\\s*(finaly)\\s*" + CODE_BLOCK);
	}

	private Pattern getSwitchCasePattern() {
		return Pattern.compile("\\s*(switch)\\s*(\\(.*\\))?\\s*(\\{(\\s*((case.*)|default\\:)(?:\\s*(?:[^\\{\\}])*\\s*"+ CODE_BLOCK +"(?:[^\\{\\}])*)\\s*)+\\})");
	}

	private Pattern getWhilePattern() {
		return Pattern.compile("(while)\\s*(\\(.*\\))\\s*((.*\\;)|" + CODE_BLOCK+")");
	}
	private Pattern getDoWhilePattern() {
		return Pattern.compile("\\s*(do)\\s*" + CODE_BLOCK + "\\s*(while)\\s*(\\(.*\\)\\;)");
	}

	@Override
	public boolean hasTextInForInMethodInSourceCode(String text, String methodName) {
		for (String methodSource : getMatchesByPattern(getMethodPattern(methodName), solutionSourceCode)) {
			methodSource = methodSource.replaceFirst(methodName, "");
			for (String forCode : getMatchesByPattern(getForPattern(), methodSource)) {
				if (Pattern.compile(text).matcher(forCode).find()) {
					return true;
				}
			}
		}
		return false;
	}

	@Override
	public boolean hasTextInWhileInMethodInSourceCode(String text, String methodName) {
		for (String methodSource : getMatchesByPattern(getMethodPattern(methodName), solutionSourceCode)) {
			methodSource = methodSource.replaceFirst(methodName, "");
			for (String code : getMatchesByPattern(getWhilePattern(), methodSource)) {
				if (Pattern.compile(text).matcher(code).find()) {
					return true;
				}
			}
		}
		return false;
	}

	@Override
	public boolean hasTextInDoWhileInMethodInSourceCode(String text, String methodName) {
		for (String methodSource : getMatchesByPattern(getMethodPattern(methodName), solutionSourceCode)) {
			methodSource = methodSource.replaceFirst(methodName, "");
			for (String code : getMatchesByPattern(getDoWhilePattern(), methodSource)) {
				if (Pattern.compile(text).matcher(code).find()) {
					return true;
				}
			}
		}
		return false;
	}

	@Override
	public boolean hasTextInSwitchInMethodInSourceCode(String text, String methodName) {
		for (String methodSource : getMatchesByPattern(getMethodPattern(methodName), solutionSourceCode)) {
			methodSource = methodSource.replaceFirst(methodName, "");
			for (String code : getMatchesByPattern(getSwitchCasePattern(), methodSource)) {
				if (Pattern.compile(text).matcher(code).find()) {
					return true;
				}
			}
		}
		return false;
	}

	@Override
	public boolean hasTextInTryCatchInMethodInSourceCode(String text, String methodName) {
		for (String methodSource : getMatchesByPattern(getMethodPattern(methodName), solutionSourceCode)) {
			methodSource = methodSource.replaceFirst(methodName, "");
			for (String code : getMatchesByPattern(geTryCatchPattern(), methodSource)) {
				if (Pattern.compile(text).matcher(code).find()) {
					return true;
				}
			}
		}
		return false;
	}
}