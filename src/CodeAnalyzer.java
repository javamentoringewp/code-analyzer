public interface CodeAnalyzer {

	boolean hasMethodInClass(String methodName);

	boolean hasMethodInClass(String methodName, Class<?> returnType, Class<?>[] arguments);

	boolean hasMethodInClass(String methodName, Class<?> returnType, Class<?>[] arguments, String[] modifiers);

	boolean isPublicMethod(String methodName, Class<?> returnType, Class<?>[] arguments);

	boolean isPrivateMethod(String methodName, Class<?> returnType, Class<?>[] arguments);

	boolean isProtectedMethod(String methodName, Class<?> returnType, Class<?>[] arguments);

	boolean isFinalMethod(String methodName, Class<?> returnType, Class<?>[] arguments);

	boolean isStaticMethod(String methodName, Class<?> returnType, Class<?>[] arguments);

	boolean hasStringInCode(String regex);

	boolean hasMethodInSubClassInClass(String methodName, String className);

	boolean hasFieldInSubClassInClass(String fieldName, Class<?> clazz);

	boolean hasFieldsInSubClassInClass(String className);

	boolean hasPublicFieldInSubClassInClass(String methodName, String className);

	boolean hasPublicMethodInSubClassInClass(String methodName, String className);

	boolean hasFieldInClass(String fieldName);

	boolean hasFieldInClass(String fieldName, Class<?> fieldClass);

	String getText();

	boolean hasSubClassInClass(String className);

	boolean hasPublicSubClassInClass(String className);

	boolean hasTextInMethodInSourceCode(String text, String methodName);

	boolean hasTextInForInMethodInSourceCode(String text, String methodName);

	boolean hasTextInWhileInMethodInSourceCode(String text, String methodName);

	boolean hasTextInDoWhileInMethodInSourceCode(String text, String methodName);

	boolean hasTextInSwitchInMethodInSourceCode(String text, String methodName);

	boolean hasTextInTryCatchInMethodInSourceCode(String text, String methodName);
}